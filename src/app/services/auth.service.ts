import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UsuarioModel } from '../models/usuario.model';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  url = 'https://identitytoolkit.googleapis.com/v1/';
  apikey = 'AIzaSyBwQc5ErOPvSS7V8QQ4pJUrg1sIj2DA0zU';

  usuarioToken: string;

  // Crear usuario
  // https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=[API_KEY]

  // login usuario
  // https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=[API_KEY]

  constructor(private http: HttpClient) {
    this.leerTokenLocalstorage();
  }

  logout() {
    localStorage.removeItem('token');
  }

  login(usuario: UsuarioModel) {
    const authData = {
      email: usuario.email,
      password: usuario.password,
      returnSecureToken: true,
    };

    return this.http
      .post(
        `${this.url}accounts:signInWithPassword?key=${this.apikey}`,
        authData
      )
      .pipe(
        map((resp) => {
          this.guardarTokenLocalstorage(resp['idToken']);
          return resp; // para que no lo bloque el map
        })
      );
  }

  nuevoUsuario(usuario: UsuarioModel) {
    const authData = {
      email: usuario.email,
      password: usuario.password,
      returnSecureToken: true,
    };

    return this.http
      .post(`${this.url}accounts:signUp?key=${this.apikey}`, authData)
      .pipe(
        map((resp) => {
          this.guardarTokenLocalstorage(resp['idToken']);
          return resp; // para que no lo bloque el map
        })
      );
  }

  guardarTokenLocalstorage(idToken: string) {
    this.usuarioToken = idToken;
    localStorage.setItem('token', idToken);

    const hoy: Date = new Date();
    hoy.setSeconds(3600);

    localStorage.setItem('expira', hoy.getTime().toString());
  }

  leerTokenLocalstorage() {
    // Necesito comprobar si tengo información en el localstorage
    if (localStorage.getItem('token')) {
      this.usuarioToken = localStorage.getItem('token');
    } else {
      this.usuarioToken = '';
    }

    return this.usuarioToken;
  }

  isAutenticated(): boolean {
    if (this.usuarioToken.length < 2) {
      return false;
    }

    const expire = Number(localStorage.getItem('expira'));
    const expireDate = new Date();
    expireDate.setTime(expire);

    if (expireDate > new Date()) {
      return true;
    } else {
      return false;
    }
  }
}
